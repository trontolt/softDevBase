import { FavoriteCard } from '../../../components/FavoriteCard';
import { tagsData } from '../../../constants/tagsData';

import './index.css';

export const Favorite = ({
  favoriteTagIds,
  removeTagIdFromFavorites
}: {
  favoriteTagIds: Array<number>;
  removeTagIdFromFavorites: (id: number) => void;
}) => {
  const renderFavoriteTags = () =>
    tagsData.map((tag) => {
      if (favoriteTagIds.includes(tag.id)) {
        return <FavoriteCard key={tag.id} tag={tag} removeCard={removeTagIdFromFavorites} />;
      }
    });

  return <div className="favorite">{renderFavoriteTags()}</div>;
};
