import { useContext } from 'react';
import { TagInfo } from './TagInfo';
import { Favorite } from './Favorite';
import { TagsContext } from '../../pages/Home';
import { useFavoriteTags } from '../../hooks/useFavoriteTags';

import './index.css';

interface IInfoScreen {}

export const InfoScreen = ({}: IInfoScreen) => {
  const { homeValues } = useContext(TagsContext);
  const { isFavoriteMode, selectedTag, favoriteTagIds } = homeValues;
  const { removeTagIdFromFavorites } = useFavoriteTags();

  return (
    <div className="info-screen">
      {isFavoriteMode ? (
        <Favorite
          favoriteTagIds={favoriteTagIds}
          removeTagIdFromFavorites={removeTagIdFromFavorites}
        />
      ) : (
        <TagInfo tagId={selectedTag} />
      )}
    </div>
  );
};
