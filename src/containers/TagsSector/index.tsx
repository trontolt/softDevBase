import { useContext } from 'react';
import { Tag } from '../../components/Tag';
import { TagsContext } from '../../pages/Home';

import { useFavoriteTags } from '../../hooks/useFavoriteTags';
import './index.css';

export const TagsSector = () => {
  const { homeValues, changeHomeValues } = useContext(TagsContext);
  const { selectedCategory, isFavoriteMode, favoriteTagIds, tagsData, filteredTagIds } = homeValues;
  const { addTagIdToFavorites } = useFavoriteTags();

  const selectTag = (id: number) => {
    changeHomeValues({ selectedTag: id });
  };

  const renderTags = () =>
    tagsData.map((tagData) => {
      if (tagData.topicId === selectedCategory && filteredTagIds.includes(tagData.id)) {
        const { id } = tagData;
        return (
          <Tag
            key={id}
            tagData={tagData}
            selectTag={selectTag}
            selectTagIdToFavorites={addTagIdToFavorites}
            isFavoriteMode={isFavoriteMode}
            favoriteTagIds={favoriteTagIds}
          />
        );
      }
      return null;
    });
  return <div className="tags-sector">{renderTags()}</div>;
};
