import { ITopic } from '../interfaces/tags';
import { TopicsId } from './enums';

export const topicsData: Array<ITopic> = [
  {
    id: TopicsId.agile,
    name: 'Agile',
    additionalInfo: 'Development approaches'
  },
  {
    id: TopicsId.ood,
    name: 'Принципы ООП',
    additionalInfo: 'OOP principles'
  },
  {
    id: TopicsId.algoritms,
    name: 'Алгоритмы',
    additionalInfo: 'Способы дсотижения быстродействия при вычеслении результатов'
  },
  {
    id: TopicsId.dataStructures,
    name: 'Структуры данных',
    additionalInfo: 'Способы хранения и быстрого доступа к данным'
  },
  {
    id: TopicsId.softDevLifeCycle,
    name: 'Software development lifecycle',
    additionalInfo: 'All processes to deliver product to customer'
  },
  {
    id: TopicsId.networkFundamentals,
    name: 'Network Fundamentals',
    additionalInfo: 'Discribe Main Netwroking Procecces'
  },
  {
    id: TopicsId.vanillaJS,
    name: 'JavaScript (vanilla)',
    additionalInfo: 'Classic javascript concepts'
  },
  {
    id: TopicsId.BOMDOM,
    name: 'Browser Obj Model, Document Obj Model',
    additionalInfo: 'BOM DOM main concepts'
  },
  {
    id: TopicsId.react,
    name: 'React JS',
    additionalInfo: 'ReactJs library'
  },
  {
    id: TopicsId.databases,
    name: 'Databases (design and development)',
    additionalInfo: 'Info related to databases'
  },
  {
    id: TopicsId.resMemory,
    name: 'Resource and memory management',
    additionalInfo: 'Ways to optimize and improve your app'
  }
];
