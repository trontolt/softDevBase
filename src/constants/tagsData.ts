import { agileTags } from './tags/data/agileTags';
import { algoritmTags } from './tags/data/algoritmTags';
import { lifecycleTags } from './tags/data/lifecycleTags';
import { OODTags } from './tags/data/OODTags';
import { networkFundamentalsTags } from './tags/data/networkFundamentalsTags';
import { dataStructuresTags } from './tags/data/dataStructuresTags';
import { vanillaJSTags } from './tags/data/vanillaJSTags';
import { BomDomTags } from './tags/data/BomDomTags';
import { reactTags } from './tags/data/reactTags';
import { databaseTags } from './tags/data/databaseTags';
import { resourceMemoryTags } from './tags/data/resourceMemoryManage';

export const tagsData = [
  ...agileTags,
  ...algoritmTags,
  ...lifecycleTags,
  ...OODTags,
  ...networkFundamentalsTags,
  ...dataStructuresTags,
  ...vanillaJSTags,
  ...BomDomTags,
  ...reactTags,
  ...databaseTags,
  ...resourceMemoryTags
];
