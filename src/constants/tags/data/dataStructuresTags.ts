import { ITag } from '../../../interfaces/tags';
import { TopicsId } from '../../enums';

export const dataStructuresTags: Array<ITag> = [
  {
    id: 14,
    title: 'Структуры данных',
    definition:
      'Это способы хранения (группировки) однотипных или логически связанных данных и доступа к ним',
    shortInfo: `
    <table cellpadding="5" cellspacing="0" border="1">
      <tr>
        <th>Data strcucture</th>
        <th>Definition</th>
        <th>Pros +</th>
        <th>Cons -</th>
      </tr>
      <tr>
        <td>Linked List</td>
        <td>
          <mark>{node, ponter} --> {node, pointer} --> {node, ponter} -->...</mark>(однонаправленный,
          двунаправленный). 1) Head (First node) 2) Chain (rest of nodes) 3) Tail (Last node)
        </td>
        <td>
          <ul>
            <li>Добавление</li>
            <li>удаление</li>
            <li>Размер может быть не фиксированный</li>
          </ul>
        </td>
        <td>
          <ul>
            <li>Занимает больше памяти (хранит указатели)</li>
            <li>Получение, поиск</li>
          </ul>
        </td>
      </tr>
      <tr>
        <td>Array</td>
        <td>
          <mark>[1,2,3,5,7,8,2]</mark>Значения идентифицируются по индексу. Поиск в памяти быстр - место
          в памяти первого элемента плюс индекс элемента. Если многомерный массив - это матрица
        </td>
        <td>
          <ul>
            <li>Доступ к элементам за константное время</li>
            <li>Хранение</li>
            <li>Поиск</li>
          </ul>
        </td>
        <td>
          <ul>
            <li>Добавление</li>
            <li>Удаление</li>
          </ul>
        </td>
      </tr>
      <tr>
        <td>Queue (FIFO)</td>
        <td>
          <mark>{head, next} --> {node, next} --> {tail, next}</mark>Первый вошел первый вышел (напр.
          event queue)
        </td>
        <td rowspan="2">Легкое добавление/удаление</td>
        <td rowspan="2">Лимитированное использование</td>
      </tr>
      <tr>
        <td>Stack (FILO)</td>
        <td>
          <mark>{head, next} --> {node, next} --> {tail, next}</mark>Первый вошел последним вышел (напр.
          процессор, браузер)
        </td>
      </tr>
      <tr>
        <td>Hash Table (Object)</td>
        <td>
          <mark>{key: value, key2: value, key3: value}</mark>Функция позволяет помещать и брать значения
          по хэшу (ключу). Также избегает коллизий чтобы значения не записывались в один участок памяти
        </td>
        <td>
          <ul>
            <li>Быстрое добавление</li>
            <li>Быстрое удаление</li>
          </ul>
        </td>
        <td>Избежание коллизий замедляет</td>
      </tr>
      <tr>
        <td>Tree</td>
        <td rowspan="2">
          Node:
          <mark>{data, childrens: [Node1, Node2]} --> Node2: { data, chldrens:[Node3]} --></mark>
          Деревья это структура данных, в которой все кроме рутового элемента имеют родителя. Каждая
          нода может содержать правого и левого ребенка а также соседа. В бинарном дереве справа идут
          только бОльшие дети, а слева мЕньшие
        </td>
        <td rowspan="2">Быстрый поиск</td>
        <td rowspan="2">Несбалансированное добавление</td>
      </tr>
      <tr>
        <td>Binary Tree</td>
      </tr>
      <tr>
        <td>Graph</td>
        <td>
          <mark>graph = {}; <br />graph.a = ['b',c']; graph.b = ['f']<br /></mark> граф - это набор
          вершин, а также ребер, которые соединяют эти вершины (ребра имеют вес)
        </td>
        <td>Быстрый поиск кратчайшего пути</td>
        <td></td>
      </tr>
      <tr>
        <td>Матрица</td>
        <td><mark>[[0,1,0,0,1],[0,1,1,0,0],[1,1,1,0,1]]</mark> Массив массивов</td>
      </tr>
      <tr>
        <td>Map</td>
        <td>
          <mark
            >let map = new Map(); <br />
            map.set("1", "str1");</mark
          >
          Коллекция ключ, значение, в которой ключи могут быть любого типа
        </td>
      </tr>
      <tr>
        <td>Set</td>
        <td>
          <mark>let set = new Set();<br />set.add({ name: "John" });</mark> Как и массив - коллекция
          значений, только каждое значение уникально (может быть только одно)
        </td>
      </tr>
    </table>
`,
    additionalInfo: '',
    topicId: TopicsId.dataStructures
  }
];
