import { ITag } from '../../../interfaces/tags';
import { TopicsId } from '../../enums';

export const OODTags: Array<ITag> = [
  {
    id: 4,
    title: 'Принципы ООП',
    definition:
      'Принципы ООП и проектирования позволяют создавать и поддерживать масштабируемые приложения',
    shortInfo: 'SOLID, YAGNI, DRY, KISS, Big Design Upfront, Avoid Premature Optimization',
    additionalInfo: '',
    topicId: TopicsId.ood
  },
  {
    id: 5,
    title: 'SOLID',
    definition: 'Принцип ООП',
    shortInfo: `
    <table border="1">
  <th>Definition</th>
  <th>Wrong</th>
  <th>Correct</th>
  <tr>
    <td>
      S - single responsobility (каждый класс реализует один функционал, через него проходит одна
      ось изменений. Декомпозиция, AntiGodObject)
    </td>
    <td>
      <pre>
            class Animal { 
              constructor(name: string){ } 
              getAnimalName() { } 
              saveAnimalToDB(a: Animal) { }
            </pre
      >
    </td>
    <td>
      <pre>
              class Animal { 
                constructor(name: string){ }
                getAnimalName() { }  
            } 
            class AnimalDB {
                getAnimal(a: Animal) { } 
                saveAnimal(a: Animal) { } 
            }
            </pre
      >
    </td>
  </tr>
  <tr>
    <td>
      O - open-closed (открытость к расширению, но закрытость к изменениям. Если классу нужно
      добавить функционал - делать это добавлением другого класса, с помощью композиции и
      наследования (или модулю, или функции). Ненужно регрессионное тестирование)
    </td>
    <td>
      <pre>
              const animalSound = (animals) => {
              for(let i = 0; i <= animals.length; i++) {
                  if(animals[i].name === 'lion') {
                      return 'roar';
                    }
                  if(animals[i].name === 'mouse') {
                      return 'squeak';
                    }
                  if(animals[i].name === 'snake') {
                      return 'hiss';
                    }
              }
          }
          </pre
      >
    </td>
    <td>
      <pre>
              const animalSound = (animals) => {
                for(let i = 0; i <= animals.length; i++) {
                    animals[i].makeSound(); //implement this in each animal
                }
            }
            </pre
      >
    </td>
  </tr>
  <tr>
    <td>
      L - Liskov Substitution Если функция работает с классом то должна и работать и с его
      наследником. (Наследуемый класс может только добавлять, а не удалять или изменять поведение
      родителя.)
    </td>
    <td>
      <pre>
        class People {
          phraze = "Hello";
          constructor(greetings) {
            this.greetings = greetings;
          }
          talk() {
            if (this.greetings) {
              return this.phraze + this.greetings;
            }
            return this.phraze;
          }
        }
        
        class Children extends People {
          <mark>talk = false;</mark>
        }
        
        const Anton = new People("Guys");
        const AntonJR = new Children();
        
        function personSay(person) {
          console.log(person.talk());
        }
        
        personSay(Anton);
        personSay(AntonJR);
          </pre>
    </td>
    <td>
      <pre>
              function talk() {
                if (this.greetings) {
                  return this.phraze + this.greetings;
                }
                return this.phraze;
              }
              
              class People {
                phraze = "Hello";
                constructor(greetings) {
                  this.greetings = greetings;
                  this.talk = talk;
                }
              }
              
              class Children extends People {
                phraze = "Children cant talk";
              }
              
              const Anton = new People("Guys");
              const AntonJR = new Children();
              
              function personSay(person) {
                console.log(person.talk());
              }
              
              personSay(Anton);
              personSay(AntonJR);
            </pre
      >
    </td>
  </tr>
  <tr>
    <td>
      I - interface segregation (Выделение общеиспользуемых частей кода (методов) в отдельные
      интерфейсы и имплементация уже их. Устранение не используемых частей)
    </td>
    <td>
      <pre>
          interface Shape {
          <mark>drawCircle();</mark>
          <mark>drawSquare();</mark>
          
        }   

        class Circle implements Shape {
          drawCircle(){ .... }
          drawSquare(){ .... }
        }

        class Square implements Shape {
          drawCircle(){ .... }
          drawSquare(){ .... }
      }
      </pre>
    </td>
    <td>
      <pre>
        interface ICircle {
          <mark>drawCircle();</mark>
      }
      interface ISquare {
          <mark>drawSquare();</mark>
      }

      class Circle implements ICircle {
        drawCircle() { .... }
    }
    class Square implements ISquare {
        drawSquare() { .... }
    }
      </pre>
    </td>
  </tr>
  <tr>
    <td>
      D - Dependency inversion - (Модули верхнего уровня не зависят от модулей нижнего. Модули
      нижнего уровня не зависят от деталей)
    </td>
    <td>
      <pre>
        function pronounceName() {
          console.log("My name is $&#123; this.name} $&#123;this.secondName}");
        }
        
        class Person {
          constructor(name, secondName) {
            this.name = name;
            this.secondName = secondName;
          }
          sayHello() {
            console.log("Hello");
          }
          <mark>sayName = pronounceName;</mark>
        }
        
        const oleg = new Person("Oleg", "Ivanov");
        oleg.sayName();
      </pre>
    </td>
    <td>
      <pre>
        const pronounceName = (name, secondName) => {
          console.log("My name is $&#123; name} $&#123; secondName} ");
        };
        
        const sayFullName = (name, secondName) => {
          pronounceName(name, secondName);
        };
        
        class Person {
          constructor(name, secondName) {
            this.name = name;
            this.secondName = secondName;
          }
          sayHello() {
            console.log("Hello");
          }
          sayName() {
            <mark>sayFullName(this.name, this.secondName);</mark>
          }
        }
        
        const oleg = new Person("Oleg", "Ivanov");
        oleg.sayName();
      </pre>
    </td>
  </tr>
</table>
 `,
    additionalInfo: '',
    topicId: TopicsId.ood
  },
  {
    id: 6,
    title: 'DRY, KISS, YAGNI and etc.',
    definition: 'Принципы ООП',
    shortInfo: `<ul>
        <li>DRY - Don't repeat yourself (Не писать повторяющиеся части кода)</li>
        <li>KISS - Keep it simple stupid (Писать простые решение, избегать усложнения кода)</li>
        <li>YAGNI - You Aren't Gonna Need It (Удалять неиспользуемые блоки кода)</li>
        <li>Big Design Upfront - Сначала проектирование и план, а после написание кода</li>
        <li>
          Avoid Premature Optomization - Проводить оптимизацию приложения после реализации, а не во время
        </li>
        <li>Бритва Окамма - Избегать ненужных сущностей</li>
      </ul>`,
    additionalInfo: '',
    topicId: TopicsId.ood
  }
];
