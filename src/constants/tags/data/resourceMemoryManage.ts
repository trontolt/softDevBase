import { ITag } from '../../../interfaces/tags';
import { getTopicPrefixId } from '../../../utils/main';
import { TopicsId } from '../../enums';

const getId = getTopicPrefixId(TopicsId.resMemory);

export const resourceMemoryTags: Array<ITag> = [
  {
    id: getId(0),
    title: 'Memory leaks',
    definition:
      'Утечки памяти - это память, которая больше не нужна приложению, но по каким-либо причинам не возвращена в операционную систему или в пул свободной памяти. JS есть garbage collector, который работает по алгоритму "пометь-очисти", тоесть сборщик мусора строит список корней (глобальные переменные, на которые хранятся ссылки в коде), далее рекурсивно проверяются их дети. Все до чего можно добраться из корня не считается мусором. Остальные участки памяти можно считать мусором, от которого освобождается память',
    shortInfo: `<ul>
    Основные виды утечек памяти:
    <li>
      Случайные глобальные переменные - напр. <br />
      <code>function foo() { this.variable = "global var" }</code>
    </li>
    <li>
      Забытые таймеры или коллбеки - напр.<br />
      <code>
        setInterval(function() { var node = document.getElementById('Node'); if(node) { <br />
        node.innerHTML = JSON.stringify(someResource)); } }, 1000);
      </code>
    </li>
    <li>
      Ссылки на элементы DOM снаружи - напр.<br />
      <code>
        var elements = { image: document.getElementById('image') }; <br />
        function doStuff() { image.src = 'http://some.url/image'; } <br />
        function removeButton() { document.body.removeChild(document.getElementById('button')); }
      </code>
    </li>
    <li>
      Замыкания - напр. <br />
      <code>
        var theThing = null; <br />
        var replaceThing = function () {<br />
        var originalThing = theThing; <br />
        var unused = function () { <br />
        if (originalThing) <br />
        console.log("hi"); <br />
        };<br />
        theThing = { <br />
        longStr: new Array(1000000).join('*'), <br />
        someMethod: function () {<br />
        console.log(someMessage);<br />
        }<br />
        }; };<br />
        setInterval(replaceThing, 1000);
      </code>
    </li>
  </ul>
  `,
    additionalInfo: '',
    topicId: TopicsId.resMemory
  },
  {
    id: getId(1),
    title: 'Chrome Leaks Detecting',
    definition:
      'Google Chrome может помочь найти утечки памяти и тормозящие загрузку файлы. Для этого есть вкладки в DevTools - Memory и Performance',
    shortInfo: `
    <ul>
        <li>
            Memory - Heap Snapshot (позволяет сделать снимок использумеой памяти в моменте, и после
            манипуляций с вашим приложением сделать контрольный snapshot, или несколько, и сравнить их
            выбрав Comparison)
        </li>
        <li>
            Allocation instrumentation on timeline - Позволяет включить запись и испоьзуя Ваше приложение
            отследить как во времени возрастает использование памяти. Если синие графики возрастают - значит
            имеется увеличение использования памяти
        </li>
        <li>Во вкладке Performance - можно отследить время загрузки файлов и функций</li>
    </ul>
    `,
    additionalInfo: '',
    topicId: TopicsId.resMemory
  },
  {
    id: getId(2),
    title: 'Web  Workers/ Service Workers',
    definition:
      'Сложные процессы, которые выполняет код, такие как обработка изображений, конвертация файлов, расчеты матриц и формул и т.д можно выносить в веб воркеры, которые не блокируют основной поток, а выполняются в отдельном потоке. Service Worker - решает проблему с плохой связью интернет (hhtps протокол или localhost)',
    shortInfo: `
    <ul>
        <li>
        Web Workers - Позволяют создать дополнительный поток для выполнения сложных задач в параллельном
        режиме и разгрузки основного потока
        </li>
        <li>
        Service Workers - позволяют создать PWA (progressive web app) из вашего приложения. Сервис
        воркер встраивается в приложение и становится прослойкой между приложением и интернетом, позволяя
        кешировать файлы, данные и запросы, что дает оффлайн функционал. А также снижает нагрузку на
        сервер. Приложение становится кроссплатформенным.
        </li>
    </ul>`,
    additionalInfo: '',
    topicId: TopicsId.resMemory
  },
  {
    id: getId(3),
    title: 'Performance optimization',
    definition:
      'Оптимизация производительности - это действия, направленные на уменьшение веса бандла, увеличения скорости загрузки приложения и производительности',
    shortInfo: `
    <ul>
        <li>Уменьшение количества запросов к серверу</li>
        <li>Использование алгоритмов для ьыстрой работы функций</li>
        <li>Выбор библиотек сс хорошим рейтингом, быстродействием и безопасностью</li>
        <li>
            Проводить оптимизации в конце разработки фичи, так как они могут сделать код менее читаемым
        </li>
        <ul>
            <b>Для client side:</b>
            <li>Optimization page loading</li>
            <li>
            правильный порядок загрузки статических файлов и хранение их на content delivery netwroks
            (CDN, cloudflare, amazon aws, Fstly...)
            </li>
            <li>Bundle files together (объединение файлов)</li>
            <li>Minifying</li>
            <li>Optimization of images</li>
            <li>Removing duplicate code</li>
            <li>Optimization of styles</li>
            <li>кеширование файлов, Lazy Loading</li>
        </ul>
        <ul>
            <b>Для server side:</b>
            <li>Optimization of database queries</li>
            <li>Optimization of speed exequting requet etc...</li>
        </ul>
    </ul>`,
    additionalInfo: '',
    topicId: TopicsId.resMemory
  }
];
