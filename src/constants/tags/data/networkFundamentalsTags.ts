import { ITag } from '../../../interfaces/tags';
import { TagsId, TopicsId } from '../../enums';

export const networkFundamentalsTags: Array<ITag> = [
  {
    id: 11,
    title: 'Definitions',
    definition: 'Networking parts',
    shortInfo: `
        <ul>
          <li>IP - адресс компьютера</li>
          <li>
            :Port - Порт(адресс) приложения в компьютере по ip
            <ul>
              <li>135-139 - порты windows</li>
              <li>21 - FTP сервер</li>
              <li>25 - почта</li>
              <li>110 - почта (получение всех писем)</li>
              <li>80 - WEB серверы</li>
              <li>3128, 8080 - Прокси серверы</li>
            </ul>
          </li>
          <li>IP + :Port = Socket</li>
          <li>Шлюз - Роутер, Маршрутизатор (в локальной сети)</li>
          <li>
            Маска подсети - Определяет принадледит ли получатель к конкретной локальной сети, если нет то
            отправляет на другой шлюз
          </li>
          <li>
            TCP - Протокол транспортного уровня. Устанавливает соединение и отвечает за гарантрированную
            доставку пакетов (если пакет не дошел - переотправляет)
          </li>
          <li>
            UDP - Протокол транспортного уровня без постоянного соединения, работает быстрей, но не
            гарантированно доставляет пакеты (хорош для видеосвязи, аудио, так как нужно передавать текущие
            пакеты, а утерянные нет смысла досылать)
          </li>
          <li>
            HTTP - HyperText Transfer Protocol (Для передачи гипертестовых документов в формате HTML и
            других произольных данных)
          </li>
          <li>FTP - File Transfer Protocol (Для передачи файлов)</li>
          <li>
            DNS - (Domain Name System «система доменных имён» Отвечает за получение информации о доменах.
            Осуществляет поиск IP по имени хоста)
          </li>
          <li>Хост - любой компьютер, отвечающий на запрос</li>
          <li>HTTP, FTP - работают через TCP протокол транспорта, DNS - через UDP</li>
        </ul>
        `,
    additionalInfo: ``,
    topicId: TopicsId.networkFundamentals
  },
  {
    id: 12,
    title: 'Модели уровней взаимодействия',
    definition: 'Описывает модели уровней взаимодействия сетевых устройств',
    shortInfo: `
          <div>
            <p>Как происходит отправка?</p>
            Источник отправки -> канал -> получатель
            <p>Виды расслыки:</p>
            1) Одноадресная 2) Многоадресная 3) Широковещательная (Broadcasting)
            <p>В основе принципов взаимодействия сетевых устройств лежит такая модель уровней:</p>
            <table cellpadding="5" cellspacing="0" border="1">
              <tr>
                <th>OSI</th>
                <th></th>
                <th>TCP/IP (DOT) \n UDP/IP</th>
              </tr>
              <tr>
                <td>7. Приложений</td>
                <td>
                  Обмен данными между программами <br />
                  HTTP, DNS, DHSP, FTP, IMAP ...
                </td>
                <td rowspan="3">4. Приложений</td>
              </tr>
              <tr>
                <td>6. Представлений</td>
                <td>Стандарты форматов файлов <br />.GIF .PNG ,JPEG ...</td>
              </tr>
              <tr>
                <td>5. Сеансовый</td>
                <td>Организация сеансов и возобновление прерванных</td>
              </tr>
              <tr>
                <td>4. Транспортный</td>
                <td>TCP и UDP протоколы с заголовками (IP adress, :Port)<br> для сегментации и обмена данными</td>
                <td>3. Транспортный</td>
              </tr>
              <tr>
                <td>3. Сетевой</td>
                <td>
                  Функции обмена частями данных (маршрутизатор, коммутатор)<br />
                  IPv4, IPv6, ICMv4, ICMv6
                </td>
                <td>2.Интернет уровень</td>
              </tr>
              <tr>
                <td>2. Канальный</td>
                <td>
                  Способы обмена, схемы, mac adress (свитч) <br />
                  PPP, FrameRelay, Ethernet
                </td>
                <td rowspan="2">1. Уровень сетевого доступа</td>
              </tr>
              <tr>
                <td>1. Физический</td>
                <td>Методы передачи данный (сетевой адаптер)</td>
              </tr>
            </table>
          </div>
        `,
    additionalInfo: '',
    topicId: TopicsId.networkFundamentals
  },
  {
    id: TagsId.networkAuthProtocolModel,
    title: 'Kerberos, NTLM, Digest, Basic',
    definition: 'Модели сетевых протоколов аутентификации',
    shortInfo: `
    <table cellpadding="5" cellspacing="0" border="1">
        <tr>
        <th>Model</th>
        <th>Flow</th>
        </tr>
        <tr>
        <td>Kerberos</td>
        <td>
            <canvas class="kerberosDiagram" width="600" height="420"></canvas>
        </td>
        </tr>
        <tr>
        <td>
            NTLM <br />
            (NT Lan Manager)
        </td>
        <td>
            User --> Request to connection --> Server --> response with Code for user, to encrypt it -->
            <br />
            user encrypt code using password hash --> user send encrypted hash to server --> <br />
            server check answer --> server give access to resources
        </td>
        </tr>
        <tr>
        <td>Basic</td>
        <td>
            Client enters credentials --> Encrypted via Base64 login and pass send in headers or query
            <br />
            with request to server --> Server Decrypt Credentials and allow user acces to resources
            (better way use https)
        </td>
        </tr>
        <tr>
        <td>Bearer</td>
        <td>
            Using POST request client send credentails to Tokens Server endpoint --> <br />
            Server respond with token --> user use received token in headers in every request to server
        </td>
        </tr>
        <tr>
        <td>Digest <br />(multy request)</td>
        <td>
            User try to get some from server (or login) --> Server response with 401 Unauthorized and headers: <br /> 
            www-Auth: nonce 'somekey' --> user resend another request with Auth header <strong> nonce key </strong> --> 200
        </td>
        </tr>
    </table>
        `,
    additionalInfo: '',
    topicId: TopicsId.networkFundamentals
  }
];
