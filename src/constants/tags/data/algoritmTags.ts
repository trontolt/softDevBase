import { ITag } from '../../../interfaces/tags';
import { TopicsId } from '../../enums';

export const algoritmTags: Array<ITag> = [
  {
    id: 7,
    title: 'Алгоритмы',
    definition: 'Это набор последовательных действий для решения задачи',
    shortInfo: `
        <ul>
          <li style="display: inline-block; margin-right: 1rem">
            <p>
              Линейный <br />
              поиск O(n)
            </p>
            t
            <div
              style="
                background: white;
                width: 50px;
                height: 50px;
                border-left: 1px dotted #555;
                border-bottom: 1px dotted #555;
              "
            >
              <svg viewBox="0 0 50 50" class="chart">
                <polyline
                  fill="none"
                  stroke="#0074d9"
                  stroke-width="2"
                  points="
                    00,50
                    20,45
                    40,40
                    50,37.5
                  "
                />
              </svg>
            </div>
            <span style="text-align: end; width: 50px; display: inline-block">n</span>
          </li>
          <li style="display: inline-block; margin-right: 1rem">
            <p>
              Бинарный <br />
              поиск О(log2n)
            </p>
            t
            <div
              style="
                background: white;
                width: 50px;
                height: 50px;
                border-left: 1px dotted #555;
                border-bottom: 1px dotted #555;
              "
            >
              <svg viewBox="0 0 50 50" class="chart">
                <polyline
                  fill="none"
                  stroke="#0074d9"
                  stroke-width="2"
                  points="
                    00,50
                    10,43
                    20,40
                    25,38
                    30,37
                    40,36
                    50,35  
                  "
                />
              </svg>
            </div>
            <span style="text-align: end; width: 50px; display: inline-block">n</span>
          </li>
          <li style="display: inline-block; margin-right: 1rem">
            <p>
              Сортировка<br />
              выбором О(n<sup>2</sup>)
            </p>
            t
            <div
              style="
                background: white;
                width: 50px;
                height: 50px;
                border-left: 1px dotted #555;
                border-bottom: 1px dotted #555;
              "
            >
              <svg viewBox="0 0 50 50" class="chart">
                <polyline
                  fill="none"
                  stroke="#0074d9"
                  stroke-width="2"
                  points="
                    00,50
                    10,47
                    20,43
                    30,35
                    40,20
                    50,00
                  "
                />
              </svg>
            </div>
            <span style="text-align: end; width: 50px; display: inline-block">n</span>
          </li>
          <li style="display: inline-block; margin-right: 1rem">
            <p>
              Сортировка <br />
              пузырьковая О(n<sup>2</sup>)
            </p>
            t
            <div
              style="
                background: white;
                width: 50px;
                height: 50px;
                border-left: 1px dotted #555;
                border-bottom: 1px dotted #555;
              "
            >
              <svg viewBox="0 0 50 50" class="chart">
                <polyline
                  fill="none"
                  stroke="#0074d9"
                  stroke-width="2"
                  points="
                    00,50
                    10,45
                    20,35
                    30,10
                    34,00
                  "
                />
              </svg>
            </div>
            <span style="text-align: end; width: 50px; display: inline-block">n</span>
          </li>
          <li style="display: inline-block; margin-right: 1rem">
            <p>
              Сортировка <br />
              быстрая Хоара <br />
              О(n Log n)
            </p>
            t
            <div
              style="
                background: white;
                width: 50px;
                height: 50px;
                border-left: 1px dotted #555;
                border-bottom: 1px dotted #555;
              "
            >
              <svg viewBox="0 0 50 50" class="chart">
                <polyline
                  fill="none"
                  stroke="#0074d9"
                  stroke-width="2"
                  points="
                00,50
                10,40
                20,30
                30,20
                40,10
                50,00 
              "
                />
              </svg>
            </div>
            <span style="text-align: end; width: 50px; display: inline-block">n</span>
          </li>
          <li>
            Для графов:
            <ul>
              <li style="display: inline-block; margin-right: 1rem">
                <p>
                  Поиск <br />
                  в ширину <br />
                  O(|V|+|E|) <br />
                  V - число ребер, <br />
                  Е - числов вершин
                </p>
              </li>
              <li <li style="display: inline-block">алгоритм Дейкстры <br />для поиска кратчайшего пути</li>
            </ul>
          </li>
          <li>
            Для деревьев:
            <ul>
              <li style="display: inline-block; margin-right: 1rem">
                <p>Рекурсивно</p>
              </li>
              <li <li style="display: inline-block">Итеративно</li>
            </ul>
          </li>
          <li>Алгоритм кеширования</li>
        </ul>  
        `,
    additionalInfo: '',
    topicId: TopicsId.algoritms
  },
  {
    id: 8,
    title: 'Алгоритмы поиска',
    definition: 'Позволяют совершить поиск данных максимально эффективно',
    shortInfo: `
        <ul>
          <li>
            Линейный поиск - начиная с первого элемента, последовательно сравниваем с искомым элементом
          </li>
          <li>
            Бинарный поиск - Делим пополам отсортированный массив, если элемент по порядку находится в
            правом подмассиве то делим его пополам и повторяем данную процедуру пока на найдем искомый
            элемент
          </li>
          <li>
          Для графов:
            <ul>
              <li>
                Поиск в ширину - Проходим по графу и ищем, есть ли в графе путь к искомой вершине, 
                тоесть берем искомый элемент и смотрим есть ли он в связях первого элемента графа, если 
                есть то нашли, если нет то ищем дальше и пушим в очередь поиска
              </li>
              <li>
                Алгоритм Дейкстры - похож на поиск в ширину, но учитывается и длина ребер, для поиска кратчайшего пути
              </li>
            </ul>
          </li>
          <li>
          Для деревьев: 
            <ul>
              <li>
                Рекурсивно
              </li>
              <li>
                Итеративно
              </li>
            </ul>
          </li>
        </ul>
        `,
    additionalInfo: '',
    topicId: TopicsId.algoritms
  },
  {
    id: 9,
    title: 'Алгоритмы сортировки',
    definition: 'Позволяют эффективно выполнить сортировку ',
    shortInfo: `
        <ul>
          <li>
            Сортировка выбором - находим минимальный элемент и меняем его с первым, далее начиная со второго
            находим минимальный и меняем со вторым и т.д
          </li>
          <li>
            Пузырьковая сортировка - В двойном цикле проходимся по всем элементам массива, если следующий
            меньше предыдущего, то меняем их местами, таким образом элементы плавно всплывают
          </li>
          <li>
            Быстрая сортировка (Хоара) - Выбираем в массиве опорное число, например посредине, проходим по
            массиву и сравниваем с этим числом, если элемент меньше опорного то засовываем в левый массив,
            если больше то в правый, если равный то в третий массив, эту же процедуру проделать для всех
            подмассивов, в конце склеиваем их в один
          </li>
        </ul>
        `,
    additionalInfo: '',
    topicId: TopicsId.algoritms
  }
];
