import { ITag } from '../../../interfaces/tags';
import { TagsId, TopicsId } from '../../enums';

export const lifecycleTags: Array<ITag> = [
  {
    id: TagsId.softDevLifeCycle,
    title: 'Software Development Lifecycle',
    definition: 'Весь цикл создания продукта или его части до поставки конечному пользователю',
    shortInfo: `
        <div>
          <canvas class="continiousDiagram" width="800" height="800"></canvas>
        </div>
        `,
    additionalInfo: `Continious integration - Практика разработки ПО, заключается в постоянном слиянии рабоих копий в основную 
        ветвь разработки. Частые автоматизированные сборки для выявления дефектов`,
    topicId: TopicsId.softDevLifeCycle
  }
];
