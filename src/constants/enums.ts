export enum DescriptionType {
  shortDescription = 0,
  longDescription = 1
}

export enum TopicsId {
  agile = 0,
  ood = 1,
  algoritms = 2,
  dataStructures = 3,
  softDevLifeCycle = 4,
  networkFundamentals = 5,
  vanillaJS = 6,
  BOMDOM = 7,
  react = 8,
  databases = 9,
  resMemory = 10
}

export enum TagsId {
  softDevLifeCycle = 10,
  networkAuthProtocolModel = 13
}
