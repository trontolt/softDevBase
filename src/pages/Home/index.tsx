import { createContext, useState } from 'react';
import { HeaderHome } from '../../components/HeaderHome';
import { Aside } from '../../containers/Aside';
import { InfoScreen } from '../../containers/InfoScreen';
import { TagsSector } from '../../containers/TagsSector';
import { DescriptionType } from '../../constants/enums';
import { tagsData } from '../../constants/tagsData';
import './index.css';
import { ITag } from '../../interfaces/tags';

const defaultState = {
  selectedCategory: 0 as number,
  tagsData: tagsData as Array<ITag>,
  selectedTag: 0 as number,
  favoriteTagIds: [] as [] | Array<number>,
  isDescriptionModalOpen: false as boolean,
  filteredTagIds: tagsData.map((tag) => tag.id) as Array<number>,
  isFavoriteMode: false,
  descriptionModalType: DescriptionType.shortDescription as DescriptionType
};

type IHomeState = typeof defaultState;
type IStateValue<T> = {
  [Key in keyof T]: T[Key];
};

export const TagsContext = createContext<{
  homeValues: IHomeState | null;
  changeHomeValues: any;
}>(null);

export const Home = () => {
  const [homeValues, setHomeValues] = useState<IHomeState>(defaultState);

  const changeHomeValues = (homeValue: IStateValue<IHomeState>) => {
    setHomeValues({ ...homeValues, ...homeValue });
  };

  const changeInfoScreenMode = () => {
    setHomeValues({ ...homeValues, isFavoriteMode: !homeValues.isFavoriteMode });
  };

  return (
    <TagsContext.Provider
      value={{
        homeValues,
        changeHomeValues
      }}>
      <div className="home">
        <HeaderHome changeInfoScreenMode={changeInfoScreenMode} />
        <div className="home--body">
          <Aside />
          <div className="body--right-side">
            <TagsSector />
            <InfoScreen />
          </div>
        </div>
      </div>
    </TagsContext.Provider>
  );
};
