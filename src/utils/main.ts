export const getTopicPrefixId = (prefix: number) => (id: number) => {
  const stringId = '' + prefix + id;
  return +stringId;
};
