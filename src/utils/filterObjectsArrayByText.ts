export const filterObjectsArrayByText = (
  array: Array<Object>,
  filterPhraze: string
): Array<Object> => {
  if (filterPhraze) {
    const lowerPhraze = filterPhraze.toLowerCase();
    const filteredArray = array.filter((item: any) => {
      let isAnyValueHasPhraze = false;
      const itemsKeys = Object.keys(item);

      itemsKeys.forEach((key) => {
        if (typeof item[key] === 'string' && item[key].toLowerCase().includes(lowerPhraze)) {
          isAnyValueHasPhraze = true;
        }
      });

      if (isAnyValueHasPhraze) {
        return true;
      }
    });
    return filteredArray;
  } else {
    return array;
  }
};
