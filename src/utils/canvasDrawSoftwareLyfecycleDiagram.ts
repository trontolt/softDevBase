import { drawArrow } from './canvasDrawArrow';

export const extractLyfecycleDiagram = () => {
  const allDiagrams = document.getElementsByClassName('continiousDiagram') as any;
  const myCanvas = allDiagrams[allDiagrams.length - 1];

  const canvasWidth = 800;
  const canvasHeight = 800;
  myCanvas.width = canvasWidth;
  myCanvas.height = canvasHeight;

  var ctx = myCanvas.getContext('2d') as CanvasRenderingContext2D;
  ctx.font = '18px Roboto';
  ctx.beginPath();
  ctx.rect(5, 210, 450, 200);
  ctx.lineWidth = 8;
  ctx.strokeStyle = 'yellow';
  ctx.closePath();
  ctx.stroke();
  ctx.beginPath();
  ctx.rect(5, 425, 450, 100);
  ctx.lineWidth = 8;
  ctx.strokeStyle = 'gray';
  ctx.closePath();
  ctx.stroke();

  ctx.fillText('Planning', 0, 20);
  drawArrow(ctx, 20, 30, 20, 60, 3, 'black');
  ctx.fillText('Define Requirements', 0, 80);
  drawArrow(ctx, 20, 90, 20, 120, 3, 'black');
  ctx.fillText('Design and prototyping', 0, 140);
  drawArrow(ctx, 20, 150, 20, 220, 3, 'black');
  ctx.fillText('Software Development', 0, 240);
  drawArrow(ctx, 90, 250, 123, 290, 3, 'black');
  ctx.fillText('Push/Merge request', 110, 310);
  drawArrow(ctx, 130, 320, 130, 350, 3, 'black');
  ctx.fillText('Job Build', 110, 370);
  drawArrow(ctx, 190, 365, 210, 365, 3, 'green');
  ctx.fillText('Job Tests', 220, 370);

  drawArrow(ctx, 295, 365, 320, 345, 3, 'red');
  ctx.fillText('Failure', 310, 337);
  drawArrow(ctx, 330, 320, 310, 270, 3, 'red');
  ctx.fillText('Report', 280, 260);
  drawArrow(ctx, 270, 250, 180, 235, 3, 'red');

  drawArrow(ctx, 290, 368, 320, 385, 3, 'green');
  ctx.fillText('Success', 300, 403);
  drawArrow(ctx, 320, 410, 290, 430, 3, 'green');
  ctx.fillText('Job Deploy', 200, 445);
  drawArrow(ctx, 190, 450, 120, 480, 3, 'green');
  ctx.fillText('Development env', 20, 500);
  drawArrow(ctx, 220, 453, 205, 475, 3, 'green');
  ctx.fillText('QA', 180, 500);
  drawArrow(ctx, 250, 453, 255, 473, 3, 'green');
  ctx.fillText('Stage', 245, 500);
  drawArrow(ctx, 285, 450, 340, 480, 3, 'green');
  ctx.fillText('Production', 325, 500);

  ctx.fillText('Continious integration', 480, 300);
  ctx.fillText('Continious delivery', 480, 460);
  ctx.fillText('With approval (manual)', 470, 480);
  ctx.fillText('Continious deployment', 480, 510);
  ctx.fillText('Without approval (automatic)', 470, 530);

  drawArrow(ctx, 230, 540, 230, 570, 3, 'green');
  ctx.fillText('Operations/Maintanance', 150, 590);
};
