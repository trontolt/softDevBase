import { drawArrow } from './canvasDrawArrow';

export const extractKerberosDiagram = () => {
  const allDiagrams = document.getElementsByClassName(
    'kerberosDiagram'
  ) as HTMLCollectionOf<HTMLCanvasElement>;
  const myCanvas = allDiagrams[allDiagrams.length - 1];

  const canvasWidth = 600;
  const canvasHeight = 420;
  myCanvas.width = canvasWidth;
  myCanvas.height = canvasHeight;

  var ctx = myCanvas.getContext('2d') as CanvasRenderingContext2D;
  ctx.font = '18px Roboto';
  ctx.beginPath();
  ctx.rect(50, 50, 200, 50);
  ctx.lineWidth = 5;
  ctx.strokeStyle = 'gray';
  ctx.closePath();
  ctx.stroke();
  ctx.fillText('Authentication server AS', 60, 80);

  drawArrow(ctx, 60, 300, 60, 120, 3, 'orange');
  ctx.fillText('1)Request with', 30, 270);
  ctx.fillText('encrypted key', 30, 290);

  ctx.beginPath();
  ctx.rect(50, 310, 200, 50);
  ctx.lineWidth = 5;
  ctx.strokeStyle = 'purple';
  ctx.closePath();
  ctx.stroke();
  ctx.fillText('User/Client', 110, 340);

  drawArrow(ctx, 170, 120, 170, 300, 3, 'gray');
  ctx.fillText('2)Response with', 120, 170);
  ctx.fillText('TGT (Ticket', 120, 190);
  ctx.fillText('granted ticket)', 120, 210);

  ctx.beginPath();
  ctx.rect(300, 50, 220, 50);
  ctx.lineWidth = 5;
  ctx.strokeStyle = 'gray';
  ctx.closePath();
  ctx.stroke();
  ctx.fillText('Ticket Granting Server TGS', 305, 80);

  drawArrow(ctx, 210, 300, 310, 120, 3, 'orange');
  ctx.fillText('3)Send', 200, 270);
  ctx.fillText('TGT', 200, 290);

  drawArrow(ctx, 350, 40, 150, 40, 3, 'gray');
  drawArrow(ctx, 150, 20, 190, 20, 3, 'gray');
  ctx.fillText('4)Decrypt TGT using secret key from AS', 200, 25);

  drawArrow(ctx, 350, 120, 250, 300, 3, 'gray');
  ctx.fillText('5)Token', 280, 200);

  ctx.beginPath();
  ctx.rect(450, 310, 120, 70);
  ctx.lineWidth = 5;
  ctx.strokeStyle = 'gray';
  ctx.closePath();
  ctx.stroke();
  ctx.fillText('File Server', 470, 340);

  drawArrow(ctx, 260, 320, 440, 320, 3, 'orange');
  ctx.fillText('6) Send Token to server', 270, 310);

  drawArrow(ctx, 440, 340, 260, 340, 3, 'green');
  ctx.fillText('8) Allow user use', 260, 360);
  ctx.fillText('resources of server while ', 260, 380);
  ctx.fillText('token alive', 290, 400);

  drawArrow(ctx, 480, 300, 450, 120, 3, 'gray');
  drawArrow(ctx, 470, 120, 500, 300, 3, 'gray');

  ctx.fillText('7)Encrypt Token', 440, 170);
  ctx.fillText('using TGS', 450, 190);
  ctx.fillText('server secret', 450, 210);
  ctx.fillText('key', 490, 230);
};
