export enum LSActionType {
  replace = 1,
  addUnique = 2,
  remove = 3
}

export const useLocalStorage = (): {
  getItemFromLS: (title: string) => any;
  setItemToLS: (title: string, value: any) => void;
  removeItemFromLS: (title: string) => void;
  updateArrayItemInLS: (
    title: string,
    actionName: LSActionType,
    newValue: any,
    oldValue?: any
  ) => void;
} => {
  const getItemFromLS = (title: string) => JSON.parse(localStorage.getItem(title) as string);

  const setItemToLS = (title: string, value: any) =>
    localStorage.setItem(title, JSON.stringify(value));

  const removeItemFromLS = (title: string) => {
    localStorage.removeItem(title);
  };

  const updateArrayItemInLS = (
    title: string,
    actionName: LSActionType,
    newValue: any,
    oldValue: any | undefined
  ) => {
    const itemArray = getItemFromLS(title) || [];
    switch (actionName) {
      case LSActionType.replace:
        const indexOfOldValue = itemArray.indexOf(oldValue);
        if (indexOfOldValue >= 0) {
          itemArray[indexOfOldValue] = newValue;
          localStorage.setItem(title, JSON.stringify(itemArray));
        }
        break;
      case LSActionType.addUnique:
        if (itemArray.indexOf(newValue) === -1) {
          itemArray.push(newValue);
          localStorage.setItem(title, JSON.stringify(itemArray));
        }
        break;
      case LSActionType.remove:
        const valueIndex = itemArray.indexOf(newValue);
        if (valueIndex >= 0) {
          itemArray.splice(valueIndex, 1);
          localStorage.setItem(title, JSON.stringify(itemArray));
        }
        break;
      default:
        throw new Error('Action name is not valid');
    }
  };
  return { getItemFromLS, setItemToLS, removeItemFromLS, updateArrayItemInLS };
};
