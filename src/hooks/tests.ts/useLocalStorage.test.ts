import { renderHook, act } from '@testing-library/react-hooks';
import { LSActionType, useLocalStorage } from '../useLocalStorage';

export class LocalStorageMock {
  store = {} as Storage;
  getItem(key: string) {
    return this.store[key] || null;
  }
  setItem(key: string, value: any) {
    this.store[key] = value.toString();
  }
  removeItem(key: string) {
    delete this.store[key];
  }
  clear() {
    this.store = {} as Storage;
  }
  length = Object.keys(this.store).length;
  key() {
    return 'test';
  }
}

describe('useLocalStorage hook should operate with local storage', () => {
  beforeEach(() => {
    global.localStorage = new LocalStorageMock();
  });
  const testItem = 'testItem';
  const testString = 'testValue';
  const testArray = [1, 2, 3, 6, 8, 10];
  const testUpdatedArray = [1, 2, 3, 6, 2, 10];

  test('useLocalStorage should return functions to handle actions with local storage', () => {
    const { result } = renderHook(() => useLocalStorage());
    expect(typeof result.current.getItemFromLS).toBe('function');
    expect(typeof result.current.removeItemFromLS).toBe('function');
    expect(typeof result.current.setItemToLS).toBe('function');
    expect(typeof result.current.updateArrayItemInLS).toBe('function');
  });

  test('setItemToLS and getItemFromLS should write and return item from local storage', () => {
    const { result } = renderHook(() => useLocalStorage());
    act(() => {
      result.current.setItemToLS(testItem, testString);
      expect(result.current.getItemFromLS(testItem)).toBe(testString);
    });
  });

  test('updateArrayItemInLS should update item in array item of local storage', () => {
    const { result } = renderHook(() => useLocalStorage());
    act(() => {
      result.current.setItemToLS(testItem, testArray);
      expect(result.current.getItemFromLS(testItem)).toStrictEqual(testArray);
      result.current.updateArrayItemInLS(testItem, LSActionType.replace, 2, 8);
      expect(result.current.getItemFromLS(testItem)).toStrictEqual(testUpdatedArray);
    });
  });

  test('removeItemFromLS should remove item from local storage', () => {
    const { result } = renderHook(() => useLocalStorage());
    act(() => {
      result.current.setItemToLS(testItem, testArray);
      expect(result.current.getItemFromLS(testItem)).toStrictEqual(testArray);
      result.current.removeItemFromLS(testItem);
      expect(result.current.getItemFromLS(testItem)).toBe(null);
    });
  });
});
