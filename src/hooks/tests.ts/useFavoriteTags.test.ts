import React, { useContext } from 'react';
import { renderHook, act } from '@testing-library/react-hooks';
import { LocalStorageMock } from './useLocalStorage.test';
import { useFavoriteTags } from '../useFavoriteTags';

let resultHomeValues: {} = {};

const mockChangeHomeValues = jest.fn().mockImplementation((homeValues) => {
  resultHomeValues = { ...homeValues };
  return resultHomeValues;
});

const mockUseContext = jest.fn().mockImplementation(() => ({
  homeValues: [],
  changeHomeValues: mockChangeHomeValues
}));

describe('useFavoriteTags hook should operate with local storage and Context', () => {
  beforeEach(() => {
    global.localStorage = new LocalStorageMock();
    React.useContext = mockUseContext;
  });

  afterEach(() => {
    localStorage.clear();
    jest.clearAllMocks();
  });

  test('useFavoriteTags should return functions to operate with favorite tag ids', () => {
    const { result } = renderHook(() => useFavoriteTags());
    expect(typeof result.current.addTagIdToFavorites).toBe('function');
    expect(typeof result.current.removeTagIdFromFavorites).toBe('function');
  });

  test('addTagIdToFavorites should add Tag ID to localStorage and to context', () => {
    const testId = 2;
    const { result } = renderHook(() => useFavoriteTags());
    act(() => {
      result.current.addTagIdToFavorites(testId);
    });
    expect(localStorage.getItem('favorites')).toBe(`[${testId}]`);
    expect(resultHomeValues).toStrictEqual({ favoriteTagIds: [testId] });
  });

  test('removeTagIdFromFavorites should remove Tag ID from localStorage and to context', () => {
    const testId = 4;
    const { result } = renderHook(() => useFavoriteTags());
    act(() => {
      result.current.addTagIdToFavorites(testId);
    });
    expect(localStorage.getItem('favorites')).toBe(`[${testId}]`);
    expect(resultHomeValues).toStrictEqual({ favoriteTagIds: [testId] });
    act(() => {
      result.current.removeTagIdFromFavorites(testId);
    });
    expect(localStorage.getItem('favorites')).toBe('[]');
    expect(resultHomeValues).toStrictEqual({ favoriteTagIds: [] });
  });
});
