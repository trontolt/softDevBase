import { useContext, useEffect } from 'react';
import { TagsContext } from '../pages/Home';
import { useLocalStorage, LSActionType } from './useLocalStorage';

export const useFavoriteTags = () => {
  const { changeHomeValues } = useContext(TagsContext);
  const { getItemFromLS, updateArrayItemInLS } = useLocalStorage();
  const favoriteTitle = 'favorites';

  useEffect(() => {
    const favoriteIds = getItemFromLS(favoriteTitle);
    if (favoriteIds) {
      changeHomeValues({ favoriteTagIds: favoriteIds });
    }
  }, []);

  const addTagIdToFavorites = (id: number) => {
    updateArrayItemInLS(favoriteTitle, LSActionType.addUnique, id);
    changeHomeValues({ favoriteTagIds: getItemFromLS(favoriteTitle) });
  };

  const removeTagIdFromFavorites = (id: number) => {
    updateArrayItemInLS(favoriteTitle, LSActionType.remove, id);
    changeHomeValues({ favoriteTagIds: getItemFromLS(favoriteTitle) });
  };

  return { addTagIdToFavorites, removeTagIdFromFavorites };
};
