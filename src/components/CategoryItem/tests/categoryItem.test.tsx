import { CategoryItem } from '../index';
import { render, screen, fireEvent } from '@testing-library/react';
import React from 'react';

const mockCategory = {
  id: 1,
  name: 'Test Category',
  additionalInfo: 'Test Info'
};
const mockSelectedCategoryId = 2;

describe('Category Item test', () => {
  test('Category Item should display category name and change category after click', () => {
    render(<CategoryItem category={mockCategory} setCategoryId={()=>{}} selectedCategoryId={mockSelectedCategoryId}/>);
    expect(screen.getByText('Test Category')).toBeDefined();
  });
});
