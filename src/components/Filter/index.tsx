import { ChangeEvent, useEffect, useState } from 'react';
import { ITag } from '../../interfaces/tags';
import { filterObjectsArrayByText } from '../../utils/filterObjectsArrayByText';
import './index.css';

interface IFilter {
  toBeFilteredArray: Array<Object>;
  filteredCB: (array: any) => void;
}

export const Filter = ({ toBeFilteredArray, filteredCB }: IFilter) => {
  const [inputValue, setInputValue] = useState('');

  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    setInputValue(event.target.value);
  }

  useEffect(() => {
    const filteredArray = filterObjectsArrayByText(toBeFilteredArray, inputValue);
    const filteredIds = filteredArray.map((item: ITag) => item.id);
    filteredCB(filteredIds);
  }, [inputValue]);

  return (
    <div className="filter-container">
      <input type="text" value={inputValue} onChange={handleChange} />
    </div>
  );
};
