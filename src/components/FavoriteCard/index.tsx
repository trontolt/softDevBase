import React, { useState } from 'react';
import { ITag } from '../../interfaces/tags';
import { ModalMode, ModalTagInfo } from '../ModalTagInfo';
import { ShortTagInfo } from '../ShortTagInfo';

import './index.css';

interface ICard {
  tag: ITag;
  removeCard: (id: number) => void;
}

export const FavoriteCard = ({ tag, removeCard }: ICard) => {
  const { title, definition, shortInfo, id } = tag;

  const [isModalInfoOpen, setModalInfoOpen] = useState(false);
  const [infoMode, setInfoMode] = useState(ModalMode.shortInfo);

  const openTagModal = (e: React.SyntheticEvent) => {
    e.stopPropagation();
    setModalInfoOpen(true);
  };
  const closeTagModal = (e: React.SyntheticEvent) => {
    e.stopPropagation();
    setModalInfoOpen(false);
  };

  return (
    <div key={id} className="favorite--card" onClick={openTagModal}>
      <button className="card--delete-btn" onClick={() => removeCard(id)}>
        x
      </button>
      <ShortTagInfo id={id} title={title} definition={definition} shortInfo={shortInfo} />

      {isModalInfoOpen && (
        <ModalTagInfo
          mode={infoMode}
          changeMode={setInfoMode}
          closeModal={closeTagModal}
          selectedTag={tag}
        />
      )}
    </div>
  );
};
