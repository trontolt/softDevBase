import { useEffect } from 'react';
import { TagsId } from '../../constants/enums';
import { ITag } from '../../interfaces/tags';
import { extractKerberosDiagram } from '../../utils/canvasDrawKerberosDiagram';
import { extractLyfecycleDiagram } from '../../utils/canvasDrawSoftwareLyfecycleDiagram';
import './index.css';

type IShortTagInfo = Omit<ITag, 'additionalInfo' | 'topicId'>;

export const ShortTagInfo = ({ id, title, definition, shortInfo }: IShortTagInfo) => {
  useEffect(() => {
    if (id === TagsId.softDevLifeCycle) {
      extractLyfecycleDiagram();
    }
    if (id === TagsId.networkAuthProtocolModel) {
      extractKerberosDiagram();
    }
  }, [id]);

  return (
    <>
      <h3>{title}</h3>
      <section className="info--section">{definition}</section>
      <section dangerouslySetInnerHTML={{ __html: shortInfo }}></section>
    </>
  );
};
