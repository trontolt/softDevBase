import { ITag } from '../../../interfaces/tags';
import { ShortTagInfo } from '../../ShortTagInfo';
import './index.css';

export const ShortModalModeScreen = ({ selectedTag }: { selectedTag: ITag }) => {
  const { id, title, definition, shortInfo } = selectedTag;
  return (
    <div className="short-modal-screen">
      <ShortTagInfo id={id} title={title} definition={definition} shortInfo={shortInfo} />
    </div>
  );
};
