import React from 'react';
import { ITag } from '../../../interfaces/tags';
import { ShortTagInfo } from '../../ShortTagInfo';
import './index.css';

export const FullModalModeScreen = ({ selectedTag }: { selectedTag: ITag }) => {
  const { id, title, definition, shortInfo } = selectedTag;
  return (
    <div className="full-modal-screen">
      <ShortTagInfo id={id} title={title} definition={definition} shortInfo={shortInfo} />
      <hr />
      <section dangerouslySetInnerHTML={{ __html: selectedTag.additionalInfo as string }}></section>
    </div>
  );
};
