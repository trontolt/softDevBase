import { ITag } from '../../interfaces/tags';
import './index.css';

export const Tag = ({
  tagData,
  selectTag,
  isFavoriteMode,
  selectTagIdToFavorites,
  favoriteTagIds
}: {
  tagData: ITag;
  selectTag: any;
  isFavoriteMode: boolean;
  selectTagIdToFavorites: (id: number) => void;
  favoriteTagIds: Array<number>;
}) => {
  const { id, title } = tagData;
  const processTag = (id: number) => {
    if (isFavoriteMode) {
      selectTagIdToFavorites(id);
    } else {
      selectTag(id);
    }
  };
  const getTagClass = () => {
    if (isFavoriteMode) {
      return favoriteTagIds.includes(id) ? 'tag-in-favorite' : 'tag-allowed';
    }
  };

  return (
    <div className={`tag ${getTagClass()}`} onClick={() => processTag(id)}>
      {title}
    </div>
  );
};
