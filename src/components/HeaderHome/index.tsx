import React, { useContext } from 'react';
import { Filter } from '../Filter';
import { Link } from 'react-router-dom';
import './index.css';
import { TagsContext } from '../../pages/Home';
import { ITag } from '../../interfaces/tags';

interface IHeaderHome {
  changeInfoScreenMode: () => void;
}

export const HeaderHome = ({ changeInfoScreenMode }: IHeaderHome) => {
  const { homeValues, changeHomeValues } = useContext(TagsContext);
  const { tagsData } = homeValues;

  const changeFilteredTagIds = (filteredTags: Array<number>) => {
    changeHomeValues({ filteredTagIds: filteredTags });
  };

  return (
    <header className="header">
      <div className="header--left-side">
        Software Developer's knowledge base
        <Filter toBeFilteredArray={tagsData} filteredCB={changeFilteredTagIds} />
      </div>

      <div className="header--right-side">
        <Link to={'/about'} className="right-side--link-to-about">
          About project
        </Link>
        <button onClick={changeInfoScreenMode} className="right-side--toggleModeBtn">
          Mode
        </button>
      </div>
    </header>
  );
};
