/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
const esModules = ['@agm', 'ngx-bootstrap'].join('|');
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  transform: {
    '^.+\\.(ts|js)x?$': 'ts-jest'
  },
  moduleNameMapper: {
    '\\.(css|sass)$': 'identity-obj-proxy',
    '\\.(png|jpg|webp|ttf|woff|woff2|svg|mp4)$': '<rootDir>/src/mock.js'
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/(?!(quill-mention)/)']
};
