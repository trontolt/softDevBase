# softDevBase

Software Developer's knowledge base

- {+ This project built from scratch +}
- {+ Uses webpack +}
- {+ Uses typescript +}
- {+ Uses React +}
- {+ Implemented automation of build, test runner and deploy +}
- {+ Deploy to the google cloud platform +}

```mermaid
graph TB
    A[Push code] -->|Merge Request| C{Pipeline jobs}
    C -->|Build| D[Success]
    C -->|Test| E[Success]
    E --> B[Merge to master]
    D --> B
    B --> F{Pipeline jobs}
    F --> |Build| G[Success]
    F --> |Test| I[Success]
    F --> |Deploy| H[Success]
    H --> J[<a href='https://softwaredevbase.uc.r.appspot.com/'>Cloud based site</a>]
```
